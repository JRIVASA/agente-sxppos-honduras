Attribute VB_Name = "Variables"
'Private Const ProcedimientoSQL_Limpiar = _
"IF (OBJECT_ID ('TestReturnValue#', 'P') IS NOT NULL)" & vbNewLine & _
vbTab & "DROP PROCEDURE TestReturnValue#"

'Private Const ProcedimientoSQL_Parte1 = _
"CREATE PROCEDURE TestReturnValue#" & vbNewLine & _
vbTab & "@P1 NVARCHAR(MAX)," & vbNewLine & _
vbTab & "@O1 NVARCHAR(MAX) OUTPUT" & vbNewLine & _
"AS" & vbNewLine & _
vbTab & "PRINT @P1" & vbNewLine & _
vbTab & "SET @O1 = 'JNDFJDNKNKNKFNKJKJKFNKJKJD'"

'Private Const ProcedimientoSQL_Base = ProcedimientoSQL_Parte1

Public Const ProcedimientoSQL_Limpiar = _
"IF (OBJECT_ID ('CopyTable#', 'P') IS NOT NULL)" & vbNewLine & _
vbTab & "DROP PROCEDURE CopyTable#"

Public Const ProcedimientoSQL_Dependencia1 = _
"IF TYPE_ID(N'TestTableType') IS NULL" & vbNewLine & _
vbTab & "CREATE Type TestTableType AS TABLE (ObjectID INT)"

Public Const ProcedimientoSQL_Parte1 = _
"CREATE PROCEDURE [dbo].[CopyTable#]" & vbNewLine & _
vbTab & "@DBName SYSNAME" & vbNewLine & _
vbTab & ",@Schema SYSNAME" & vbNewLine & _
vbTab & ",@TableName SYSNAME" & vbNewLine & _
vbTab & ",@IncludeConstraints BIT = 1" & vbNewLine & _
vbTab & ",@IncludeIndexes BIT = 1" & vbNewLine & _
vbTab & ",@NewTableSchema SYSNAME" & vbNewLine & _
vbTab & ",@NewTableName SYSNAME = NULL" & vbNewLine & _
vbTab & ",@NewDBName SYSNAME = NULL" & vbNewLine & _
vbTab & ",@UseSystemDataTypes BIT = 0" & vbNewLine & _
vbTab & ",@CreateTableAndCopyData BIT = 0" & vbNewLine & _
vbTab & ",@Script NVARCHAR(MAX) OUTPUT" & vbNewLine & _
"AS" & vbNewLine & _
"BEGIN" & vbNewLine & _
vbNewLine

Public Const ProcedimientoSQL_Parte2 = _
vbTab & "TRY" & vbNewLine & _
vbNewLine & _
vbTab & "IF (@NewDBName IS NULL) SET @NewDBName = @DBName" & vbNewLine & _
vbNewLine & _
vbTab & "IF NOT EXISTS (SELECT * FROM SYS.Types WHERE Name = 'TestTableType')" & vbNewLine & _
vbTab & vbTab & "CREATE TYPE TestTableType AS Table (ObjectID INT)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @SQL NVARCHAR(MAX)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @MainDefinition TABLE (FieldValue VARCHAR(200))" & vbNewLine & _
vbTab & "--DECLARE @DBName SYSNAME" & vbNewLine & _
vbTab & "DECLARE @ClusteredPK BIT" & vbNewLine & _
vbTab & "DECLARE @TableSchema NVARCHAR(255)" & vbNewLine & _
vbNewLine & _
vbTab & "--SET @DBName = DB_NAME(DB_ID())" & vbNewLine

Public Const ProcedimientoSQL_Parte3 = _
vbTab & "SELECT @TableName = Name FROM SysObjects WHERE ID = OBJECT_ID(@TableName)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @ShowFields TABLE (FieldID INT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",DatabaseName VARCHAR(100)" & vbNewLine & _
vbTab & ",TableOwner VARCHAR(100)" & vbNewLine & _
vbTab & ",TableName VARCHAR(100)" & vbNewLine & _
vbTab & ",FieldName VARCHAR(100)" & vbNewLine & _
vbTab & ",ColumnPosition INT" & vbNewLine & _
vbTab & ",ColumnDefaultValue VARCHAR(100)" & vbNewLine & _
vbTab & ",ColumnDefaultName VARCHAR(100)" & vbNewLine & _
vbTab & ",IsNullable BIT" & vbNewLine & _
vbTab & ",DataType VARCHAR(100)" & vbNewLine & _
vbTab & ",MaxLength varchar(10)" & vbNewLine & _
vbTab & ",NumericPrecision INT" & vbNewLine & _
vbTab & ",NumericScale INT" & vbNewLine & _
vbTab & ",DomainName VARCHAR(100)" & vbNewLine & _
vbTab & ",FieldListingName VARCHAR(110)" & vbNewLine & _
vbTab & ",FieldDefinition CHAR(1)" & vbNewLine & _
vbTab & ",IdentityColumn BIT" & vbNewLine & _
vbTab & ",IdentitySeed INT" & vbNewLine & _
vbTab & ",IdentityIncrement INT" & vbNewLine & _
vbTab & ",IsCharColumn BIT" & vbNewLine & _
vbTab & ",IsComputed varchar(255))" & vbNewLine

Public Const ProcedimientoSQL_Parte4 = _
vbNewLine & _
vbTab & "DECLARE @HoldingArea TABLE(FldID SMALLINT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",Flds VARCHAR(4000)" & vbNewLine & _
vbTab & ",FldValue CHAR(1) DEFAULT(0))" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @PKObjectID TABLE(ObjectID INT)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @Uniques TABLE(ObjectID INT)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @HoldingAreaValues TABLE(FldID SMALLINT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",Flds VARCHAR(4000)" & vbNewLine & _
vbTab & ",FldValue CHAR(1) DEFAULT(0))" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @Definition TABLE(DefinitionID SMALLINT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",FieldValue VARCHAR(200))" & vbNewLine & _
vbNewLine

Public Const ProcedimientoSQL_Parte5 = _
vbTab & "SET @SQL=" & vbNewLine & _
vbTab & "'" & vbNewLine & _
vbTab & "USE ['+@DBName+']" & vbNewLine & _
vbTab & "SELECT DISTINCT DB_NAME()" & vbNewLine & _
vbTab & vbTab & ",TABLE_SCHEMA" & vbNewLine & _
vbTab & vbTab & ",TABLE_NAME" & vbNewLine & _
vbTab & vbTab & ",''[''+COLUMN_NAME+'']'' AS COLUMN_NAME" & vbNewLine & _
vbTab & vbTab & ",CAST(ORDINAL_POSITION AS INT)" & vbNewLine & _
vbTab & vbTab & ",COLUMN_DEFAULT" & vbNewLine & _
vbTab & vbTab & ",dobj.name AS ColumnDefaultName" & vbNewLine & _
vbTab & vbTab & ",CASE WHEN c.IS_NULLABLE = ''YES'' THEN 1 ELSE 0 END" & vbNewLine & _
vbTab & vbTab & ",DATA_TYPE" & vbNewLine & _
vbTab & vbTab & ",case CHARACTER_MAXIMUM_LENGTH when -1 then ''max'' else CAST(CHARACTER_MAXIMUM_LENGTH AS varchar) end--CAST(CHARACTER_MAXIMUM_LENGTH AS INT)" & vbNewLine & _
vbTab & vbTab & ",CAST(NUMERIC_PRECISION AS INT)" & vbNewLine & _
vbTab & vbTab & ",CAST(NUMERIC_SCALE AS INT)" & vbNewLine & _
vbTab & vbTab & ",DOMAIN_NAME" & vbNewLine & _
vbTab & vbTab & ",COLUMN_NAME + '',''" & vbNewLine & _
vbTab & vbTab & ",'''' AS FieldDefinition" & vbNewLine & _
vbTab & vbTab & ",CASE WHEN ic.object_id IS NULL THEN 0 ELSE 1 END AS IdentityColumn" & vbNewLine & _
vbTab & vbTab & ",CAST(ISNULL(ic.seed_value,0) AS INT) AS IdentitySeed" & vbNewLine & _
vbTab & vbTab & ",CAST(ISNULL(ic.increment_value,0) AS INT) AS IdentityIncrement" & vbNewLine & _
vbTab & vbTab & ",CASE WHEN st.collation_name IS NOT NULL THEN 1 ELSE 0 END AS IsCharColumn" & vbNewLine

Public Const ProcedimientoSQL_Parte6 = _
vbTab & vbTab & ",cc.definition" & vbNewLine & _
vbTab & vbTab & "FROM INFORMATION_SCHEMA.COLUMNS c" & vbNewLine & _
vbTab & vbTab & "JOIN sys.columns sc ON  c.TABLE_NAME = OBJECT_NAME(sc.object_id) AND c.COLUMN_NAME = sc.Name" & vbNewLine & _
vbTab & vbTab & "LEFT JOIN sys.identity_columns ic ON c.TABLE_NAME = OBJECT_NAME(ic.object_id) AND c.COLUMN_NAME = ic.Name" & vbNewLine & _
vbTab & vbTab & "JOIN sys.types st ON COALESCE(c.DOMAIN_NAME,c.DATA_TYPE) = st.name" & vbNewLine & _
vbTab & vbTab & "LEFT OUTER JOIN sys.objects dobj ON dobj.object_id = sc.default_object_id AND dobj.type = ''D''" & vbNewLine & _
vbTab & vbTab & "left join sys.computed_columns cc on c.TABLE_NAME=OBJECT_NAME(cc.object_id) and sc.column_id=cc.column_id" & vbNewLine & _
vbTab & vbTab & "WHERE c.TABLE_NAME = @TableName and c.TABLE_SCHEMA=@schema" & vbNewLine & _
vbTab & vbTab & "ORDER BY c.TABLE_NAME, c.ORDINAL_POSITION" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & "PRINT @SQL" & vbNewLine

Public Const ProcedimientoSQL_Parte7 = _
vbTab & "INSERT INTO @ShowFields( DatabaseName" & vbNewLine & _
vbTab & vbTab & ",TableOwner" & vbNewLine & _
vbTab & vbTab & ",TableName" & vbNewLine & _
vbTab & vbTab & ",FieldName" & vbNewLine & _
vbTab & vbTab & ",ColumnPosition" & vbNewLine & _
vbTab & vbTab & ",ColumnDefaultValue" & vbNewLine & _
vbTab & vbTab & ",ColumnDefaultName" & vbNewLine & _
vbTab & vbTab & ",IsNullable" & vbNewLine & _
vbTab & vbTab & ",DataType" & vbNewLine & _
vbTab & vbTab & ",MaxLength" & vbNewLine & _
vbTab & vbTab & ",NumericPrecision" & vbNewLine & _
vbTab & vbTab & ",NumericScale" & vbNewLine & _
vbTab & vbTab & ",DomainName" & vbNewLine & _
vbTab & vbTab & ",FieldListingName" & vbNewLine & _
vbTab & vbTab & ",FieldDefinition" & vbNewLine & _
vbTab & vbTab & ",IdentityColumn" & vbNewLine & _
vbTab & vbTab & ",IdentitySeed" & vbNewLine & _
vbTab & vbTab & ",IdentityIncrement" & vbNewLine & _
vbTab & vbTab & ",IsCharColumn" & vbNewLine & _
vbTab & vbTab & ",IsComputed)" & vbNewLine & _
vbNewLine

Public Const ProcedimientoSQL_Parte8 = _
vbTab & "EXEC sp_executesql @SQL," & vbNewLine & _
vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & "SELECT TOP 1 @TableSchema = TableOwner FROM @ShowFields" & vbNewLine & _
vbNewLine & _
vbTab & "INSERT INTO @HoldingArea (Flds) VALUES ('(')" & vbNewLine & _
vbNewLine & _
vbTab & "INSERT INTO @Definition(FieldValue) VALUES ('CREATE TABLE ' + CASE WHEN @NewTableName IS NOT NULL THEN @NewDBName + '.' + @NewTableSchema + '.' + @NewTableName ELSE @NewDBName + '.' + @TableSchema + '.' + @TableName END)" & vbNewLine & _
vbTab & "INSERT INTO @Definition(FieldValue) VALUES ('(')" & vbNewLine & _
vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbNewLine & _
vbTab & "SELECT   CHAR(10) + FieldName + ' ' +" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "CASE WHEN DomainName IS NOT NULL AND @UseSystemDataTypes = 0 THEN DomainName +" & vbNewLine & _
vbTab & vbTab & vbTab & "CASE WHEN IsNullable = 1 THEN ' NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & "Else ' NOT NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & "Else" & vbNewLine

Public Const ProcedimientoSQL_Parte9 = _
vbTab & vbTab & vbTab & "case when IsComputed is null then" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "UPPER(DataType) +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN (IsCharColumn = 1 and UPPER(DataType) <> 'TEXT') THEN '(' + CAST(MaxLength AS VARCHAR(10)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "CASE WHEN DataType = 'numeric' THEN '(' + CAST(NumericPrecision AS VARCHAR(10))+','+ CAST(NumericScale AS VARCHAR(10)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "Else" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "CASE WHEN DataType = 'decimal' THEN '(' + CAST(NumericPrecision AS VARCHAR(10))+','+ CAST(NumericScale AS VARCHAR(10)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "Else ''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN IdentityColumn = 1 THEN ' IDENTITY(' + CAST(IdentitySeed AS VARCHAR(5))+ ',' + CAST(IdentityIncrement AS VARCHAR(5)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else ''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN IsNullable = 1 THEN ' NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else ' NOT NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN ColumnDefaultName IS NOT NULL AND @IncludeConstraints = 1 THEN 'CONSTRAINT [' + replace(ColumnDefaultName,@TableName,@NewTableName) + '] DEFAULT' + UPPER(ColumnDefaultValue)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else ''" & vbNewLine

Public Const ProcedimientoSQL_Parte10 = _
vbTab & vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & vbTab & "Else" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "' as '+IsComputed+' '" & vbNewLine & _
vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & "CASE WHEN FieldID = (SELECT MAX(FieldID) FROM @ShowFields) THEN ''" & vbNewLine & _
vbTab & vbTab & "Else ','" & vbNewLine & _
vbTab & vbTab & "End" & vbNewLine & _
vbNewLine & _
vbTab & "FROM    @ShowFields" & vbNewLine & _
vbNewLine & _
vbTab & "IF @IncludeConstraints = 1" & vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "SET @SQL =" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & "SELECT  distinct  '',CONSTRAINT ['' + replace(name,@TableName,@NewTableName) + ''] FOREIGN KEY ('' + ParentColumns + '') REFERENCES ['' + ReferencedObject + '']('' + ReferencedColumns + '')''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM ( SELECT   ReferencedObject = OBJECT_NAME(fk.referenced_object_id), ParentObject = OBJECT_NAME(parent_object_id),fk.name" & vbNewLine

Public Const ProcedimientoSQL_Parte11 = _
vbTab & vbTab & vbTab & vbTab & ",   REVERSE(SUBSTRING(REVERSE((   SELECT cp.name + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "FROM   sys.foreign_key_columns fkc" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "JOIN sys.columns cp ON fkc.parent_object_id = cp.object_id AND fkc.parent_column_id = cp.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "WHERE fkc.constraint_object_id = fk.object_id   FOR XML PATH('''')   )), 2, 8000)) ParentColumns," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "REVERSE(SUBSTRING(REVERSE((   SELECT cr.name + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "FROM   sys.foreign_key_columns fkc" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "JOIN sys.columns cr ON fkc.referenced_object_id = cr.object_id AND fkc.referenced_column_id = cr.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "WHERE fkc.constraint_object_id = fk.object_id   FOR XML PATH('''')   )), 2, 8000)) ReferencedColumns" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "FROM sys.foreign_keys fk" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "inner join sys.schemas s on fk.schema_id=s.schema_id and s.name=@schema) a" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE ParentObject = @TableName" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & "PRINT @SQL" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema" & vbNewLine

Public Const ProcedimientoSQL_Parte12 = vbNewLine & _
vbTab & vbTab & vbTab & "SET @SQL =" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT distinct '',CONSTRAINT ['' + replace(c.name,@TableName,@NewTableName) + ''] CHECK '' + definition" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM sys.check_constraints c join sys.schemas s on c.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE OBJECT_NAME(parent_object_id) = @TableName" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "PRINT @SQL" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "SET @SQL =" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT DISTINCT  PKObject = cco.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM    sys.key_constraints cco" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.index_columns cc ON cco.parent_object_id = cc.object_id AND cco.unique_index_id = cc.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.indexes i ON cc.object_id = i.object_id AND cc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "join sys.schemas s on cco.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE    OBJECT_NAME(parent_object_id) = @TableName    AND  i.type = 1 AND    is_primary_key = 1" & vbNewLine

Public Const ProcedimientoSQL_Parte13 = _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "PRINT @SQL" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @PKObjectID(ObjectID)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "SET @SQL=" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT DISTINCT    PKObject = cco.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM    sys.key_constraints cco" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.index_columns cc ON cco.parent_object_id = cc.object_id AND cco.unique_index_id = cc.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.indexes i ON cc.object_id = i.object_id AND cc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "join sys.schemas s on cco.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE    OBJECT_NAME(parent_object_id) = @TableName AND  i.type = 2 AND    is_primary_key = 0 AND    is_unique_constraint = 1" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Uniques(ObjectID)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine

Public Const ProcedimientoSQL_Parte14 = _
vbTab & vbTab & vbTab & "SET @ClusteredPK = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "declare @t TestTableType" & vbNewLine & _
vbTab & vbTab & vbTab & "insert @t select * from @PKObjectID" & vbNewLine & _
vbTab & vbTab & vbTab & "declare @u TestTableType" & vbNewLine & _
vbTab & vbTab & vbTab & "insert @u select * from @Uniques" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "set @sql=" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT distinct '',CONSTRAINT '' + replace(cco.name,@TableName,@NewTableName) + CASE type WHEN ''PK'' THEN '' PRIMARY KEY '' + CASE WHEN pk.ObjectID IS NULL THEN '' NONCLUSTERED '' ELSE '' CLUSTERED '' END  WHEN ''UQ'' THEN '' UNIQUE '' END + CASE WHEN u.ObjectID IS NOT NULL THEN '' NONCLUSTERED '' ELSE '''' END" & vbNewLine & _
vbTab & vbTab & vbTab & "+ ''(''+REVERSE(SUBSTRING(REVERSE(( SELECT   c.name +  + CASE WHEN cc.is_descending_key = 1 THEN '' DESC'' ELSE '' ASC'' END + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM   sys.key_constraints ccok" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN sys.index_columns cc ON ccok.parent_object_id = cc.object_id AND cco.unique_index_id = cc.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN sys.columns c ON cc.object_id = c.object_id AND cc.column_id = c.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN sys.indexes i ON cc.object_id = i.object_id AND cc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "Where i.object_id = ccok.parent_object_id And ccok.object_id = cco.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "order by key_ordinal FOR XML PATH(''''))), 2, 8000)) + '')''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM sys.key_constraints cco" & vbNewLine & _
vbTab & vbTab & vbTab & "inner join sys.schemas s on cco.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN @U u ON cco.object_id = u.objectID" & vbNewLine

Public Const ProcedimientoSQL_Parte15 = _
vbTab & vbTab & vbTab & "LEFT JOIN @t pk ON cco.object_id = pk.ObjectID" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE    OBJECT_NAME(cco.parent_object_id) = @TableName" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50),@t TestTableType readonly,@u TestTableType readonly'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema,@t=@t,@u=@u" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "End" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "INSERT INTO @Definition(FieldValue) VALUES (')')" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "set @sql=" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & "select '' on '' + d.name + ''([''+c.name+''])''" & vbNewLine & _
vbTab & vbTab & "from sys.tables t join sys.indexes i on(i.object_id = t.object_id and i.index_id < 2)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.index_columns ic on(ic.partition_ordinal > 0 and ic.index_id = i.index_id and ic.object_id = t.object_id)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.columns c on(c.object_id = ic.object_id and c.column_id = ic.column_id)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.schemas s on t.schema_id=s.schema_id" & vbNewLine

Public Const ProcedimientoSQL_Parte16 = _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.data_spaces d on i.data_space_id=d.data_space_id" & vbNewLine & _
vbTab & vbTab & "where t.name=@TableName and s.name=@schema" & vbNewLine & _
vbTab & vbTab & "order by key_ordinal" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "IF @IncludeIndexes = 1" & vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbTab & vbTab & vbTab & "set @sql=" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT distinct '' CREATE '' + i.type_desc + '' INDEX ['' + replace(i.name COLLATE SQL_Latin1_General_CP1_CI_AS,@TableName,@NewTableName) + ''] ON '+@NewDBName+'.'+@NewTableSchema+'.'+@NewTableName+' (''" & vbNewLine & _
vbTab & vbTab & vbTab & "+   REVERSE(SUBSTRING(REVERSE((   SELECT name + CASE WHEN sc.is_descending_key = 1 THEN '' DESC'' ELSE '' ASC'' END + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM  sys.index_columns sc" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.columns c ON sc.object_id = c.object_id AND sc.column_id = c.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE  t.name=@TableName AND  sc.object_id = i.object_id AND  sc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "and is_included_column=0" & vbNewLine

Public Const ProcedimientoSQL_Parte17 = _
vbTab & vbTab & vbTab & "ORDER BY key_ordinal ASC   FOR XML PATH('''')    )), 2, 8000)) + '')''+" & vbNewLine & _
vbTab & vbTab & vbTab & "ISNULL( '' include (''+REVERSE(SUBSTRING(REVERSE((   SELECT name + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM  sys.index_columns sc" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.columns c ON sc.object_id = c.object_id AND sc.column_id = c.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE  t.name=@TableName AND  sc.object_id = i.object_id AND  sc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "and is_included_column=1" & vbNewLine & _
vbTab & vbTab & vbTab & "ORDER BY key_ordinal ASC   FOR XML PATH('''')    )), 2, 8000))+'')'' ,'''')+''''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM sys.indexes i join sys.tables t on i.object_id=t.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "join sys.schemas s on t.schema_id=s.schema_id" & vbNewLine & _
vbTab & vbTab & vbTab & "AND CASE WHEN @ClusteredPK = 1 AND is_primary_key = 1 AND i.type = 1 THEN 0 ELSE 1 END = 1   AND is_unique_constraint = 0   AND is_primary_key = 0" & vbNewLine & _
vbTab & vbTab & vbTab & "where t.name=@TableName and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50), @ClusteredPK bit'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema,@ClusteredPK=@ClusteredPK" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "End" & vbNewLine

Public Const ProcedimientoSQL_Parte18 = _
vbTab & vbTab & vbTab & "INSERT INTO @MainDefinition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT FieldValue FROM @Definition" & vbNewLine & _
vbTab & vbTab & vbTab & "ORDER BY DefinitionID ASC" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "----------------------------------" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "declare @q  varchar(max)" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "set @q=(select replace((SELECT FieldValue FROM @MainDefinition FOR XML PATH('')),'</FieldValue>',''))" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "set @script=(select REPLACE(@q,'<FieldValue>',''))" & vbNewLine & _
vbNewLine & _
"if @CreateTableAndCopyData =1" & vbNewLine & _
"BEGIN" & vbNewLine & _
vbTab & "print  @script" & vbNewLine & _
vbTab & "exec sp_executesql @script" & vbNewLine & _
vbNewLine & _
vbTab & "SELECT Stuff(( SELECT ', '+FieldName FROM @ShowFields FOR XML PATH('') ), 1, 2, '')" & vbNewLine & _
vbNewLine & _
vbTab & "declare @s nvarchar(max)" & vbNewLine & _
vbTab & "set @s=" & vbNewLine & _
vbTab & "'SET IDENTITY_INSERT ' + @NewTableName + ' ON" & vbNewLine & _
vbTab & "Insert ' + @NewTableName + ' ( ' +" & vbNewLine & _
vbTab & "(SELECT Stuff(( SELECT ', '+FieldName FROM @ShowFields FOR XML PATH('') ), 1, 2, ''))" & vbNewLine

Public Const ProcedimientoSQL_Parte19 = _
vbTab & "+ ')" & vbNewLine & _
vbTab & "SELECT * from ' + @TableName + '" & vbNewLine & _
vbTab & "SET IDENTITY_INSERT ' + @NewTableName + ' OFF '" & vbNewLine & _
vbTab & "print  @s" & vbNewLine & _
vbTab & "exec sp_executesql @s" & vbNewLine & _
"End" & vbNewLine & _
"END try" & vbNewLine & _
"-- ##############################################################################################################################################################################" & vbNewLine & _
"BEGIN CATCH" & vbNewLine & _
vbTab & "BEGIN" & vbNewLine & _
vbTab & vbTab & vbTab & "Print '***********************************************************************************************************************************************************'" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorNumber               : ' + CAST(ERROR_NUMBER() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorSeverity             : ' + CAST(ERROR_SEVERITY() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorState                : ' + CAST(ERROR_STATE() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorLine                 : ' + CAST(ERROR_LINE() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorMessage              : ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print '***********************************************************************************************************************************************************'" & vbNewLine & _
vbTab & "End" & vbNewLine & _
vbTab & "SET @Script=''" & vbNewLine & _
"END CATCH" & vbNewLine

Public Const ProcedimientoSQL_Base = ProcedimientoSQL_Parte1 & ProcedimientoSQL_Parte2 & ProcedimientoSQL_Parte3 _
& ProcedimientoSQL_Parte4 & ProcedimientoSQL_Parte5 & ProcedimientoSQL_Parte6 & ProcedimientoSQL_Parte7 _
& ProcedimientoSQL_Parte8 & ProcedimientoSQL_Parte9 & ProcedimientoSQL_Parte10 & ProcedimientoSQL_Parte11 _
& ProcedimientoSQL_Parte12 & ProcedimientoSQL_Parte13 & ProcedimientoSQL_Parte14 & ProcedimientoSQL_Parte15 _
& ProcedimientoSQL_Parte16 & ProcedimientoSQL_Parte17 & ProcedimientoSQL_Parte18 & ProcedimientoSQL_Parte19
