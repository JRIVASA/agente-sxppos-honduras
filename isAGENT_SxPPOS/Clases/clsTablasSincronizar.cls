VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTablasSincronizar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mVarCodigo                                                                          As Integer
Private mVarProceso                                                                         As Integer
Private mVarDescripcion                                                                     As String
Private mVarMensaje                                                                         As String
Private mVarMensajeEliminar                                                                 As String
Private mVarConceptos                                                                       As String
Private mVarManejaInv                                                                       As Boolean
Private mTablas                                                                             As Collection

Property Get Codigo() As Integer
    Codigo = mVarCodigo
End Property

Property Get Proceso() As Integer
    Proceso = mVarProceso
End Property

Property Get Descripcion() As String
    Descripcion = mVarDescripcion
End Property

Property Get Mensaje() As String
    Mensaje = mVarMensaje
End Property

Property Get MsgEliminar() As String
    MsgEliminar = mVarMensajeEliminar
End Property

Property Get Conceptos() As String
    Conceptos = mVarConceptos
End Property

Property Get Tablas() As Collection
    Set Tablas = mTablas
End Property

Friend Function AgregarTablaSincronizar(pRs As ADODB.Recordset) As clsTablasSincronizar
    
    Dim mTbl As clsTablaSincro
    
    mVarCodigo = pRs!cu_codtablassyncronizar
    mVarProceso = pRs!nu_proceso
    mVarDescripcion = pRs!cu_descripcion
    mVarMensaje = pRs!cu_mensaje
    mVarMensajeEliminar = pRs!cu_mensajeEliminar
    mVarConceptos = pRs!cu_concepto
    mVarManejaInv = pRs!bs_maneja_inventario
    
    Set mTablas = New Collection
    
    Call AgregarTabla(pRs!cu_tabla_maestra, etblMaestra, pRs!cu_campob_maestra, pRs!cu_camposExepcion)
    Call AgregarTabla(pRs!cu_tabla_tr_pend, etblPendiente, pRs!cu_campob_tr_pend, pRs!cu_camposExepcion)
    
    If Trim(pRs!cu_tabla_TR) <> "" Then
        Call AgregarTabla(pRs!cu_tabla_TR, etblTransaccion, pRs!cu_campob_TR, pRs!cu_camposExepcionTr)
    End If
    
    Set AgregarTablaSincronizar = Me
    
End Function

Private Sub AgregarTabla(pNombre As String, pTipo As eTipoTablaSincro, pCamposB, pCamposEx)
    
    Dim mTbl As clsTablaSincro
    
    Set mTbl = New clsTablaSincro
    mTablas.Add mTbl.AgregarTabla(pNombre, pTipo, pCamposB, pCamposEx)
    
End Sub

Friend Function BuscarWhereConceptos() As String
    
    Dim mConcepto
    Dim mWhere As String
    
    mConcepto = Split(mVarConceptos, "|")
    
    For i = 0 To UBound(mConcepto)
        mWhere = mWhere & IIf(Trim(mWhere) <> "", ",", "") & "'" & mConcepto(i) & "'"
    Next
    
    BuscarWhereConceptos = mWhere
    
End Function
