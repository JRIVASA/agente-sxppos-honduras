VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsActualizaSucursal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mConexionO                                                                              As ADODB.Connection
Private mArchivos                                                                               As Collection
Private mVarCodigo                                                                              As String
Private mVarManejaSuc                                                                           As Boolean
Private mVarManejaPos                                                                           As Boolean
Private mVarSucursalPrincipal                                                                   As Boolean
Private mVarSoloPos                                                                             As Boolean
Private mVarMoverArchivo                                                                        As Boolean
Private mVarCarpetaDatos                                                                        As String
Private mVarCarpetaProcesado                                                                    As String
Private mVarCarpetaNoProcesado                                                                  As String
Private mTablasSincro                                                                           As Collection

Event Estatus(pEstatus As String)

Public Sub ActualizarDatos(pCnOrigen As ADODB.Connection, pCnSucursal As ADODB.Connection, pTblSincronizar As Collection, pRuta As String, pSucursal As String, Optional pSucPrincipal As Boolean = False, Optional pSoloPos As Boolean = False, Optional pMoverArchivo As Boolean)
    
    Dim mTbl As clsTablasSincronizar
    Dim miArchivo As String
    Dim mTabla As String, mCorrelativo As String
    Dim mManejaPos As Boolean
   
    ConfigurarClase pCnOrigen, pCnSucursal, pTblSincronizar, pRuta, pSucursal, pSucPrincipal, pSoloPos, pMoverArchivo

    For n = 1 To mArchivos.Count
        miArchivo = mArchivos(n)
        If miArchivo <> "" Then
             mTabla = "": mCorrelativo = ""
             If NombreTablaArchivo(miArchivo, mTabla, mCorrelativo) Then
                Set mTbl = BuscarTablaSincro(mTabla)
                If Not mTbl Is Nothing Then
                    DoEvents
                    RaiseEvent Estatus("Actualizando " & mTbl.Descripcion)
                    Select Case mTbl.Proceso
                        Case 0
                            If Trim(UCase(miArchivo)) Like "TR_PENDIENTE_CODIGO*" Then
                                ActualizarRsSucursal pCnSucursal, mTbl, mCorrelativo, "tr_pendiente_codigo", "ma_codigos"
                            Else
                                ActualizarRsSucursal pCnSucursal, mTbl, mCorrelativo
                            End If
                            
                        Case 1, 4
                            ActualizarRsSucursal pCnSucursal, mTbl, mCorrelativo
                            
                        Case 2, 3
                            If Not pSoloPos Then ActualizarRsSucursal pCnSucursal, mTbl, mCorrelativo, , True, True
                    End Select
                End If
            End If
         End If
     Next
     
End Sub

Private Sub ActualizarRsSucursal(pCnSucursal As ADODB.Connection, pTbl As clsTablasSincronizar, pCorrelativo As String, Optional pTblSec, Optional pTblSecMa, Optional pSoloAdm As Boolean = False)
    
    Dim mTbl As clsTablaSincro
    Dim mTblPend As clsTablaSincro
    Dim mTblTr As clsTablaSincro
    
    Set mTbl = BuscarTablaActualizar(pTbl, etblMaestra)
    Set mTblPend = BuscarTablaActualizar(pTbl, etblPendiente)
    Set mTblTr = BuscarTablaActualizar(pTbl, etblTransaccion)
    
    Select Case pTbl.Proceso
        Case 3 'MANEJAN TR
            If UCase(mTbl.Nombre) Like "*MA_ODC*" Then
                ActualizarODC pCnSucursal, mTblPend, mTblTr, pCorrelativo
                If ExisteArchivo(NombreArchivoRs("MA_ODC_ANULADAS", pCorrelativo)) Then AnularOdc pCnSucursal, mTblPend, mTblTr, pCorrelativo
            Else
                ActualizarTr pCnSucursal, mTblPend, mTbl, mTblTr, pCorrelativo
            End If
        Case Else
            If UCase(pTbl.Descripcion) Like "*USUARIO*" Then   '
                ActualizarArchivo pCnSucursal, mTbl.Nombre, mTblPend.Nombre, mTblPend.Campos, pCorrelativo, True, pSoloAdm
            Else
                If IsMissing(pTblSec) Then
                    ActualizarArchivo pCnSucursal, mTbl.Nombre, mTblPend.Nombre, mTbl.Campos, pCorrelativo, , pSoloAdm
                Else
                    ActualizarArchivo pCnSucursal, CStr(pTblSecMa), CStr(pTblSec), mTbl.Campos, pCorrelativo, , pSoloAdm
                End If
            End If
    End Select
    
End Sub

Private Sub ActualizarArchivo(pCn As ADODB.Connection, pTblMa As String, pTblPend As String, pCampos As Collection, pCorrelativo As String, Optional pESUsuario As Boolean = False, Optional pSoloAdm As Boolean = False)
    
    Dim mRs As ADODB.Recordset
    Dim mError As Boolean, mManejaCajas As Boolean
    Dim mArrProc As clsArreglo, mArrNoProc As clsArreglo
    
    Set mArrProc = New clsArreglo
    Set mArrNoProc = New clsArreglo
    Set mRs = New ADODB.Recordset
    
    If mVarMoverArchivo Then
        
        'Debug.Print Dir(NombreArchivo(pTblPend, pCorrelativo, mvarCarpetaProcesado))
        'Debug.Print Dir(NombreArchivo(pTblPend, pCorrelativo, mvarCarpetaDatos))
        'Debug.Print Dir(NombreArchivo(pTblPend, pCorrelativo, mvarCarpetaNoProcesado))
        
        If Dir(NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaProcesado)) <> "" And Dir(NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaDatos)) <> "" Then
            Kill NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaDatos)
            'No Reprocesar. 15/01/2015 Luis Arraga
            Exit Sub
        ElseIf Dir(NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaNoProcesado)) <> "" And Dir(NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaDatos)) <> "" Then
            Kill NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaNoProcesado)
            'Ya que no fue procesado anteriormente, que se intente procesar de nuevo, pero debemos borrar
            'de la carpeta no procesados para que no "se atasque" en la carpeta de datos.
        End If
    End If
    
    mRs.Open NombreArchivoRs(pTblPend, pCorrelativo), "provider=mspersist;"
    
    If ExisteCampoRs(mRs, "id") Then mRs.Sort = "id"
    
    Do While Not mRs.EOF
        
        DoEvents
        
        mError = False
        
        pCn.BeginTrans
        
        ' Si no es solo pos actualizo mi VAD10.
        
        If Not mVarSoloPos Then
            If Not ActualizarRegistro(pCn, mRs, pTblMa, pCampos, pCorrelativo) Then
                mError = True
            End If
            ' si la tabla es de usuario inserto en tablas de configuracion usuarios y no error
            If pESUsuario And Not mError Then
                If InsertarDatosUsuario(pCn, "ORG" & mRs!CODUSUARIO, pCorrelativo, "Conf_menu_user", "clave_user", EsActualizar(mRs), pCampos) Then
                    If Not InsertarDatosUsuario(pCn, "ORG" & mRs!CODUSUARIO, pCorrelativo, "estruc_org", "clave", EsActualizar(mRs), pCampos) Then
                        mError = True
                    End If
                Else
                    mError = True
                End If
            End If
        End If
                
        'actualizo vad si manejo sucursales y no solo actualizo pos inserto en tr_Pend
        If mVarManejaSuc And Not mVarSoloPos And Not mError Then
            'inserto en tr_pend solo si no es sucursal principal y no ocurrio error
            If Not InsertarRegistro(pCn, mRs, pTblPend, pCampos, pCorrelativo) Then
                mError = True
            End If
        End If
        
        ' Si no manejo sucursal o solopos si es solo pos se trata de la sucursal origen actualizandose
        ' si no maneja sucursal ya que si manejo sucursal inserte en tr_pend y cuando corra el agente yo
        ' en la sucursal me actualizare el pos
        ' y por supuesto tengo que manejar pos no haber tenido error y no ser solo adm ejemplo ma_dptos,grupos
        
        If (Not mVarManejaSuc Or mVarSoloPos) And mVarManejaPos And Not mError And Not pSoloAdm Then
            mManejaCajas = True
            'inserto en tr_pend vad20 y si es productos o codigos actualizo vad20 y no ocurrio error
            If UCase(pTblMa) Like "*MA_PRODUCTO*" Or UCase(pTblMa) Like "*MA_CODIGO*" Then
                mManejaCajas = False
                ' Cree la Funci�n ExisteTabla pero me di cuenta de que esto no es necesario, ya que
                ' todas las tablas deber�an existir en VAD20. Llegue con un problema aqu� por falta de documentaci�n
                ' y por culpa del bendito LIKE caim�n que esta atras... queriendo sincronizar MA_PRODUCTOS_WEB.
                ' Lo unico que necesitaba era colocar mi tabla en el nu_Proceso 2 para que solo sincronice en VAD10.
                'If ExisteTabla(pTblMa, pCn, "VAD20") Then
                    If Not ActualizarRegistro(pCn, mRs, pTblMa, pCampos, pCorrelativo, "VAD20") Then
                        mError = True
                    End If
                'End If
            End If
            If Not mError Then
                'If ExisteTabla(pTblPend, pCn, "VAD20") Then
                    If Not InsertarRegistro(pCn, mRs, pTblPend, pCampos, pCorrelativo, True, mManejaCajas) Then
                        mError = True
                    End If
                'End If
            End If
        End If
        
        If Not mError Then
            mArrProc.AddItem mRs.Bookmark
            pCn.CommitTrans
        Else
            mArrNoProc.AddItem mRs.Bookmark
        End If
        
        mRs.MoveNext
        
    Loop
    
    If mArrNoProc.NumItems > 0 Then
        If mVarMoverArchivo Then
            MoverArchivo NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaNoProcesado)
            If pESUsuario Then
                MoverArchivo NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaDatos), NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaNoProcesado)
                MoverArchivo NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaDatos), NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaNoProcesado)
            End If
        Else
            CopiarArchivosProcesado mRs, pTblPend, pCorrelativo, mArrProc, mArrNoProc
            If pESUsuario Then
                CopiarArchivo NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaDatos), NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaNoProcesado), True
                CopiarArchivo NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaDatos), NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaNoProcesado), True
            End If
        End If
    Else
        If mVarMoverArchivo Then
            MoverArchivo NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaProcesado)
            If pESUsuario Then
                MoverArchivo NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaDatos), NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaProcesado)
                MoverArchivo NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaDatos), NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaProcesado)
            End If
        Else
            CopiarArchivo NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaProcesado), True
            If pESUsuario Then
                CopiarArchivo NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaDatos), NombreArchivo("Conf_menu_user", pCorrelativo, mVarCarpetaProcesado), True
                CopiarArchivo NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaDatos), NombreArchivo("estruc_org", pCorrelativo, mVarCarpetaProcesado), True
            End If
        End If
    End If
    
End Sub

Private Function ActualizarRegistro(pCn As ADODB.Connection, pRs As ADODB.Recordset, pTablaMa As String, pCampos As Collection, pCorrelativo As String, Optional pBD As String = "VAD10") As Boolean
    
    Dim mRsMa As ADODB.Recordset
    Dim mField As ADODB.Field
    Dim mTipo As String, mTabla As String
    
    On Error GoTo Errores
    
    If Not IsMissing(pBD) Then
        mTabla = pBD & ".dbo." & pTablaMa
    Else
        mTabla = pTablaMa
    End If
    
    If EsActualizar(pRs) Then
        
        mTipo = "Actualizando Registro"
        mSql = "Select * from " & mTabla & BuscarWhere(pRs, pCampos)
        Set mRsMa = New ADODB.Recordset
        
        mRsMa.Open mSql, pCn, adOpenKeyset, adLockPessimistic, adCmdText
        
        If mRsMa.EOF Then mRsMa.AddNew
        
        For Each mField In mRsMa.Fields
            If ExisteCampoRs(pRs, mField.Name) Then
                If ValidarCampo(mField.Name, pCampos) Then
                    If Not IsNull(pRs.Fields(mField.Name).Value) Then
                        mField.Value = pRs.Fields(mField.Name).Value
                    End If
                End If
            End If
        Next
        
        mRsMa.Update
        
    Else
        mTipo = "Eliminado Registro"
        mSql = "Delete from " & mTabla & BuscarWhere(pRs, pCampos)
        pCn.Execute mSql
    End If
    
    ActualizarRegistro = True
    
    Exit Function
    
Errores:
    
    pCn.RollbackTrans
    
    GrabarErrorBD mTipo, Err.Description, Err.Number, ErrActualizando, CStr(pCorrelativo), mVarCodigo, pTablaMa, BuscarValor(pRs, pCampos, 0), BuscarValor(pRs, pCampos, 1), CDate(BuscarValor(pRs, pCampos, 2))
    
End Function

Private Function InsertarRegistro(pCn As ADODB.Connection, pRs As ADODB.Recordset, pTabla As String, pCampos As Collection, pCorrida As String, Optional pEsPos As Boolean = False, Optional pCajas As Boolean = False) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mField As ADODB.Field
    Dim mCajas As Collection
    Dim mTabla As String
    
    On Error GoTo Errores
    mTabla = IIf(pEsPos, "VAD20.DBO.", "VAD10.DBO.") & pTabla
    
    If pEsPos And pCajas Then
        Set mCajas = BuscarCajas(pCn)
    Else
        Set mCajas = New Collection
        mCajas.Add ""
    End If
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open "Select * from " & mTabla & " where 1=2", pCn, adOpenKeyset, adLockPessimistic, adCmdText
    
    For i = 1 To mCajas.Count
        mRs.AddNew
            If ExisteCampoRs(mRs, "n_caja") And mCajas(i) <> "" Then mRs!n_caja = mCajas(i)
            For Each mField In pRs.Fields
                If ExisteCampoRs(mRs, mField.Name) Then
                    If ValidarCampo(mField.Name, pCampos) Then
                        mRs.Fields(mField.Name).Value = mField.Value
                    End If
                End If
            Next
        mRs.Update
    Next i
    
    InsertarRegistro = True
    Exit Function
    
Errores:
    
    pCn.RollbackTrans
    GrabarErrorBD "Insertando Registro Pend Pos", Err.Description, Err.Number, ErrActualizando, pCorrida, mVarCodigo, pTabla, BuscarValor(pRs, pCampos, 0)
    
End Function

Private Sub ActualizarTr(pCn As ADODB.Connection, pTblPend As clsTablaSincro, pTblMa As clsTablaSincro, ptbltr As clsTablaSincro, pCorrelativo As String)
    
    Dim mRs As ADODB.Recordset
    Dim mArrProc As clsArreglo, mArrNoProc As clsArreglo
    Dim mArchivo As String
    Dim mActualizo As Boolean, mError As Boolean
    
    mArchivo = NombreArchivoRs(pTblPend.Nombre, pCorrelativo)
    
    Set mArrProc = New clsArreglo
    Set mArrNoProc = New clsArreglo
    Set mRs = New ADODB.Recordset
    
    mRs.Open mArchivo, "provider=mspersist;"
    mRs.Filter = "C_LOCALIDAD='" & mVarCodigo & "' "
    
    If Not mRs.EOF Then
        Do While Not mRs.EOF
            mActualizo = False: mError = False
            pCn.BeginTrans
            If InsertarDatosTr(pCn, mRs!c_documento, pCorrelativo, ptbltr.Nombre, ptbltr.Campos) Then
                If InsertarDatosTr(pCn, mRs!c_documento, pCorrelativo, pTblMa.Nombre, pTblMa.Campos) Then
                    If mVarManejaSuc Then
                        If Not InsertarRegistro(pCn, mRs, pTblPend.Nombre, pTblPend.Campos, pCorrelativo) Then
                           mError = True
                        End If
                    End If
                    If Not mError Then
                        pCn.CommitTrans
                        mActualizo = True
                    End If
                End If
            End If
            If mActualizo Then
                mArrProc.AddItem mRs.Bookmark
            Else
                mArrNoProc.AddItem mRs.Bookmark
            End If
            mRs.MoveNext
        Loop
        
        If mArrNoProc.NumItems > 0 Then
            CopiarArchivosProcesado mRs, pTbl.Nombre, pCorrelativo, mArrProc, mArrNoProc
            CopiarArchivo NombreArchivo(ptbltr.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(ptbltr.Nombre, pCorrelativo, mVarCarpetaNoProcesado), True
            CopiarArchivo NombreArchivo(pTblMa.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTblMa.Nombre, pCorrelativo, mVarCarpetaNoProcesado), True
            
        Else
            If mVarMoverArchivo Then
                MoverArchivo NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaProcesado)
                MoverArchivo NombreArchivo(ptbltr.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(ptbltr.Nombre, pCorrelativo, mVarCarpetaProcesado)
                MoverArchivo NombreArchivo(pTblMa.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTblMa.Nombre, pCorrelativo, mVarCarpetaProcesado)
            Else
                CopiarArchivo NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaProcesado), True
                CopiarArchivo NombreArchivo(ptbltr.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(ptbltr.Nombre, pCorrelativo, mVarCarpetaProcesado), True
                CopiarArchivo NombreArchivo(pTblMa.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTblMa.Nombre, pCorrelativo, mVarCarpetaProcesado), True
            End If
        End If
    End If
    
End Sub

Private Sub AnularOdc(pCn As ADODB.Connection, pTbl As clsTablaSincro, ptbltr As clsTablaSincro, pCorrelativo As String)
    
    Dim mRs As ADODB.Recordset, mRsDestino As New ADODB.Recordset
    
    On Error GoTo Error
    
    mArchivo = NombreArchivoRs("MA_ODC_ANULADAS", pCorrelativo)
    
    Set mRs = New ADODB.Recordset
    mRs.Open mArchivo, "provider=mspersist;"
    
    While Not mRs.EOF
        mRsDestino.Open "UPDATE MA_ODC SET c_status = 'ANU' WHERE c_DOCUMENTO = '" & mRs!c_documento & "'", pCn, adOpenStatic, adLockPessimistic, adCmdText
        'Debug.Print mRs!c_documento & " " & mRs!c_status & " " & mRs!c_codproveedor & " " & mRs!c_codlocalidad & " " & mRs!c_descripcion
        mRs.MoveNext
    Wend
    
    MoverArchivo NombreArchivo("MA_ODC_ANULADAS", pCorrelativo, mVarCarpetaDatos), NombreArchivo("MA_ODC_ANULADAS", pCorrelativo, mVarCarpetaProcesado)
    
    Exit Sub
    
Error:
    
    pCn.RollbackTrans
    MoverArchivo NombreArchivo("MA_ODC_ANULADAS", pCorrelativo, mVarCarpetaDatos), NombreArchivo("MA_ODC_ANULADAS", pCorrelativo, mVarCarpetaNoProcesado)
    
End Sub

Private Sub ActualizarODC(pCn As ADODB.Connection, pTbl As clsTablaSincro, ptbltr As clsTablaSincro, pCorrelativo As String)
    
    Dim mRs As ADODB.Recordset
    Dim mArrProc As clsArreglo, mArrNoProc As clsArreglo
    Dim mArchivo As String
    Dim mActualizo As Boolean, mError As Boolean
    
    mArchivo = NombreArchivoRs(pTbl.Nombre, pCorrelativo)
    
    Set mArrProc = New clsArreglo
    Set mArrNoProc = New clsArreglo
    Set mRs = New ADODB.Recordset
    
    mRs.Open mArchivo, "provider=mspersist;"
    mRs.Filter = BuscarFiltro(pCn)
    
    If Not mRs.EOF Then
        Do While Not mRs.EOF
            mActualizo = False: mError = False
            pCn.BeginTrans
            
            If InsertarDatosTr(pCn, mRs!c_documento, pCorrelativo, "tr_odc", ptbltr.Campos) Then
                If InsertarDatosTr(pCn, mRs!c_documento, pCorrelativo, "tr_odc_descuentos", ptbltr.Campos) Then
                    If InsertarDatosTr(pCn, mRs!c_documento, pCorrelativo, "ma_odc", pTbl.Campos) Then
                        If mVarManejaSuc Then
                            If Not InsertarRegistro(pCn, mRs, pTbl.Nombre, pTbl.Campos, pCorrelativo) Then
                                mError = True
                            End If
                        End If
                        If Not mError Then
                            pCn.CommitTrans
                            mActualizo = True
                        End If
                    End If
                End If
            End If
            
            If mActualizo Then
                mArrProc.AddItem mRs.Bookmark
            Else
                mArrNoProc.AddItem mRs.Bookmark
            End If
            mRs.MoveNext
        Loop
        
        Debug.Print mArrNoProc.NumItems
        
        If mArrNoProc.NumItems > 0 Then
            If mVarMoverArchivo Then
                MoverArchivo NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaNoProcesado)
                MoverArchivo NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaNoProcesado)
                MoverArchivo NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaNoProcesado)
                MoverArchivo NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaNoProcesado)
            Else
                CopiarArchivosProcesado mRs, pTbl.Nombre, pCorrelativo, mArrProc, mArrNoProc
                CopiarArchivo NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaNoProcesado), True
                CopiarArchivo NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaNoProcesado), True
                CopiarArchivo NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaNoProcesado), True
            End If
        Else
            If mVarMoverArchivo Then
                MoverArchivo NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaProcesado)
                MoverArchivo NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaProcesado)
                MoverArchivo NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaProcesado)
                MoverArchivo NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaProcesado)
            Else
                CopiarArchivo NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaDatos), NombreArchivo(pTbl.Nombre, pCorrelativo, mVarCarpetaProcesado), True
                CopiarArchivo NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc", pCorrelativo, mVarCarpetaProcesado), True
                CopiarArchivo NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaDatos), NombreArchivo("tr_odc_descuentos", pCorrelativo, mVarCarpetaProcesado), True
                CopiarArchivo NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaDatos), NombreArchivo("ma_odc", pCorrelativo, mVarCarpetaProcesado), True
            End If
        End If
    End If
      
End Sub

Private Function BuscarFiltro(pCn As ADODB.Connection) As String
    
    Dim mRs As ADODB.Recordset
    Dim mAux As String
    
    mAux = "c_localidad='" & mVarCodigo & "'"
    Set mRs = BuscarRsSucursalEmpresa(mVarCodigo)
    
    If Not mRs Is Nothing Then
        Do While Not mRs.EOF
            mAux = mAux & " or c_localidad='" & mRs!cs_codlocalidad & "'"
            mRs.MoveNext
        Loop
    End If
    
    BuscarFiltro = mAux
    
End Function

Private Function InsertarDatosTr(pCn As ADODB.Connection, pDocOdc As String, pCorrelativo As String, pTabla As String, pCampos As Collection) As Boolean
    
    Dim mRsO As ADODB.Recordset
    Dim mRsD As ADODB.Recordset
    Dim mSql As String, mArchivo As String
    Dim mField As ADODB.Field
    
    On Error GoTo Errores
    
    Set mRsO = New ADODB.Recordset
    Set mRsD = New ADODB.Recordset
    
    If ExisteArchivo(NombreArchivoRs(pTabla, pCorrelativo)) Then
        mRsO.Open NombreArchivoRs(pTabla, pCorrelativo), "provider=mspersist;"
        mCampo = IIf(Trim(UCase(pTabla)) = "TR_ODC_DESCUENTOS", "cu_documento", "c_documento")
        mRsO.Filter = mCampo & "='" & pDocOdc & "' "
    Else
        mCampo = IIf(Trim(UCase(pTabla)) = "TR_ODC_DESCUENTOS", "cu_documento", "c_documento")
        mRsO.Open "Select * from " & pTabla & " where " & mCampo & "='" & pDocOdc & "'", mConexionO, adOpenForwardOnly, adLockReadOnly, adCmdText
    End If
    
    mRsD.Open "Select * from " & pTabla & " where 1=2", pCn, adOpenKeyset, adLockOptimistic, adCmdText
    
    Do While Not mRsO.EOF
        mRsD.AddNew
            CopiarRs mRsO, mRsD, pCampos
        mRsD.Update
        mRsO.MoveNext
    Loop
    
    InsertarDatosTr = True

    Exit Function
    
Errores:
    
    Debug.Print Err.Description
    pCn.RollbackTrans
    GrabarErrorBD "InsertarDatosTr", Err.Description, Err.Number, ErrActualizando, pCorrelativo, mVarCodigo, pTabla, pDocOdc
    
End Function

Private Function InsertarDatosUsuario(pCn As ADODB.Connection, pClave As String, pCorrelativo As String, pTabla As String, pCampoB, pAdd As Boolean, pCampos As Collection) As Boolean
    
    Dim mRsO As ADODB.Recordset
    Dim mRsD As ADODB.Recordset
    Dim mSql As String, mArchivo As String
    
    On Error GoTo Errores
      
    mSql = "Delete from " & pTabla & " where " & pCampoB & "='" & pClave & "'"
    pCn.Execute mSql, REG
    
    If pAdd Then
        Set mRsO = New ADODB.Recordset
        Set mRsD = New ADODB.Recordset
        
        If ExisteArchivo(NombreArchivoRs(pTabla, pCorrelativo)) Then
            mRsO.Open NombreArchivoRs(pTabla, pCorrelativo), "provider=mspersist;"
            mRsO.Filter = pCampoB & "='" & pClave & "' "
        Else
            mRsO.Open "Select * from " & pTabla & " where " & pCampoB & "='" & pClave & "' ", mConexionO, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        
        mRsD.Open "Select * from " & pTabla & " where 1=2", pCn, adOpenKeyset, adLockOptimistic, adCmdText
        
        Do While Not mRsO.EOF
            
            mRsD.AddNew
                CopiarRs mRsO, mRsD, pCampos
            mRsD.Update
            mRsO.MoveNext
        Loop
    End If
    
    InsertarDatosUsuario = True
    
    Exit Function
    
Errores:
    
    pCn.RollbackTrans
    GrabarErrorBD "InsertarDatosUsuario", Err.Description, Err.Number, ErrActualizando, pCorrelativo, mVarCodigo, pTabla, pClave
    
End Function

'*********************** rutinas apoyo *******************************************************************************
Private Sub ConfigurarClase(pCnOrigen As ADODB.Connection, pCnSucursal As ADODB.Connection, pTbls As Collection, pRuta As String, pSucursal As String, pSucPred As Boolean, pSoloPos As Boolean, pMoverArchivo As Boolean)
    
    Set mConexionO = pCnOrigen
    Set mTablasSincro = pTbls
    
    mVarCodigo = pSucursal
    mVarCarpetaDatos = pRuta
    
    If pSucPred Then
        mVarSucursalPrincipal = True
        mVarCarpetaProcesado = pRuta & "Procesados\"
        mVarCarpetaNoProcesado = pRuta & "NoProcesados\"
    Else
        mVarSucursalPrincipal = False
        mVarCarpetaProcesado = pRuta & "SUC" & pSucursal & "\Procesados\"
        mVarCarpetaNoProcesado = pRuta & "SUC" & pSucursal & "\NoProcesados\"
    End If
    
    VerificarCarpetas
    
    mVarSoloPos = pSoloPos
    mVarMoverArchivo = pMoverArchivo
    mVarManejaSuc = SucursalManejaSucursales(pCnSucursal)
    mVarManejaPos = SucursalManejaPos(pCnSucursal)
    
    Set mArchivos = BuscarArchivosActualizar(mVarCarpetaDatos)
    
    'If mArchivos.Count > 1 Then
        'Set mArchivos = OrdenarArchivos(mArchivos)
    'End If

    'MsgBox mvarCarpetaDatos
    
End Sub

Private Function BuscarTablaSincro(pTabla As String) As clsTablasSincronizar
    
    Dim mTbl As clsTablasSincronizar
    Dim mTabla As clsTablaSincro
    
    For Each mTbl In mTablasSincro
        For Each mTabla In mTbl.Tablas
            If CadenasIguales(pTabla, mTabla.Nombre) Or (CadenasIguales(pTabla, "tr_pendiente_codigo") And CadenasIguales(mTabla.Nombre, "tr_pendiente_prod")) Then
                Set BuscarTablaSincro = mTbl
                Exit Function
            End If
        Next
    Next
    
End Function

Private Function NombreArchivoRs(pTabla As String, pCorrelativo As String)
    NombreArchivoRs = mVarCarpetaDatos & pTabla & ";" & pCorrelativo & ".adtg"
    'Debug.Print NombreArchivoRs
End Function

Private Sub CopiarRs(pRsOrigen As ADODB.Recordset, pRsDestino As ADODB.Recordset, pCamposEx As Collection)
    
    Dim mField As ADODB.Field
    
    For Each mField In pRsDestino.Fields
        If ExisteCampoRs(pRsOrigen, mField.Name) Then
            If ValidarCampo(mField.Name, pCamposEx) Then
                mField.Value = pRsOrigen.Fields(mField.Name).Value
            End If
        End If
    Next
    
End Sub

Private Sub CopiarArchivosProcesado(pRs As ADODB.Recordset, pTblPend As String, pCorrelativo As String, pArrProc As clsArreglo, pArrNoProc As clsArreglo)
    
    Dim mRs As ADODB.Recordset
    Dim mRsDatos As ADODB.Recordset
    
    Set mRs = CrearRsFiltrado(pRs, pArrProc.Arreglo)
    
    If mRs.RecordCount > 0 Then
        mRs.Save NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaNoProcesado), adPersistADTG
    End If
    
    Set mRsDatos = New ADODB.Recordset
    mRsDatos.Open NombreArchivo(pTblPend, pCorrelativo), "provider=mspersist;"
    
    Set mRs = CrearRsFiltrado(mRsDatos, pArrNoProc.Arreglo)
    
    If mRs.RecordCount > 0 Then
        mRs.Save NombreArchivo(pTblPend, pCorrelativo, mVarCarpetaProcesado), adPersistADTG
    End If
    
End Sub

Private Function CrearRsFiltrado(pRs As ADODB.Recordset, pArrayBookMark) As ADODB.Recordset
    
    Dim mRs As ADODB.Recordset
    Dim mBookMark As Long
    
    Set mRs = pRs.Clone
    
    For mBookMark = 0 To UBound(pArrayBookMark)
        If Not IsEmpty(pArrayBookMark(mBookMark)) Then
            mRs.Bookmark = pArrayBookMark(mBookMark)
            mRs.Delete
            mRs.Update
        End If
    Next
    
    Set CrearRsFiltrado = mRs
    
End Function

Private Function EsActualizar(pRs As ADODB.Recordset) As Boolean
    
    If ExisteCampoRs(pRs, "TIPOCAMBIO") Then
        EsActualizar = pRs!TIPOCAMBIO <> 1
        Exit Function
    End If
    
    If ExisteCampoRs(pRs, "TIPO_CAMBIO") Then
        EsActualizar = pRs!TIPO_CAMBIO <> 1
        Exit Function
    End If
    
    If ExisteCampoRs(pRs, "ESTATUS") Then
        EsActualizar = pRs!Estatus <> 1
        Exit Function
    End If
    
End Function

Private Function BuscarValor(pRs As ADODB.Recordset, pCampos As Collection, pTipo As Integer)
    
    Dim mCampoTbl As clsCampos
    Dim mField As ADODB.Field
    Dim mValor
    
    On Error Resume Next
    
    For Each mCampoTbl In pCampos
        If mCampoTbl.Tipo = eTblBusqueda Then
            Select Case pTipo
                Case 0 'documento o codigo o llave ejemplo (c_CODIGO + C_DEPARTAMENTO)
                    If Not Trim(LCase(mCampoTbl.Nombre)) Like "*concepto*" Then
                        mValor = mValor & pRs.Fields(mCampoTbl.Nombre).Value
                    End If
                Case 1 ' concepto
                    If Trim(LCase(mCampoTbl.Nombre)) Like "*concepto*" Then
                        mValor = pRs.Fields(mCampoTbl.Nombre).Value
                    End If
                Case 2 ' fecha
                    For Each mField In pRs.Fields
                        If Trim(LCase(mField.Name)) Like "*fecha*" Then
                            mValor = pRs.Fields(mCampoTbl.Nombre).Value
                            Exit For
                        End If
                    Next
            End Select
        End If
    Next
    
    If pTipo = 2 And Not IsDate(mValor) Then mValor = "01/01/1900"
    BuscarValor = mValor
    
End Function

Private Function ValidarCampo(pCampo As String, pCampos As Collection) As Boolean
    
    Dim mCampoTbl As clsCampos
    
    For Each mCampoTbl In pCampos
        If mCampoTbl.Tipo = eTblExcepcion And CadenasIguales(mCampoTbl.Nombre, pCampo) Then
            Exit Function
        End If
    Next
    
    ValidarCampo = True
    
End Function

Private Function BuscarWhere(pRs As ADODB.Recordset, pCampos As Collection) As String
    
    Dim mCampoTbl As clsCampos
    Dim mWhere As String
    
    For Each mCampoTbl In pCampos
        If mCampoTbl.Tipo = eTblBusqueda Then
            mWhere = mWhere & IIf(Trim(mWhere) = "", " where (", " and (") & mCampoTbl.Nombre & "='" & pRs.Fields(mCampoTbl.Nombre) & "') "
        End If
    Next
    
    BuscarWhere = mWhere
    
End Function

Private Sub VerificarCarpetas()
    VerificarExisteCarpeta mVarCarpetaDatos
    VerificarExisteCarpeta mVarCarpetaProcesado
    VerificarExisteCarpeta mVarCarpetaNoProcesado
End Sub

Private Function BuscarCajas(pCn As ADODB.Connection) As Collection
    
    Dim mRs As ADODB.Recordset
    Dim mCol As Collection
    Dim mCaja As String
    
    Set mRs = New ADODB.Recordset
    Set mCol = New Collection
    
    mRs.CursorLocation = adUseClient
    
    mRs.Open "Select distinct c_codigo from vad20.dbo.ma_caja ", pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    mRs.ActiveConnection = Nothing
    
    If Not mRs.EOF Then
        Do While Not mRs.EOF
            mCaja = mRs!C_codigo
            mCol.Add mCaja
            mRs.MoveNext
        Loop
    Else
        mCol.Add ""
    End If
    
    Set BuscarCajas = mCol
    
End Function

Private Function ExisteArchivo(pArchivo As String, Optional pRuta As String) As Boolean
    ExisteArchivo = Dir(pArchivo, vbArchive) <> ""
End Function
