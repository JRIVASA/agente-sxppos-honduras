Attribute VB_Name = "modAgente"
Declare Function ExitWindowsEx& Lib "user32" (ByVal uFlags&, ByVal dwReserved&)

Public Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long

Public Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" _
    (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long

Private Declare Function GetComputerName Lib "kernel32" _
    Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, _
        ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Enum eEstadoSucursal
    eSucNoActiva
    eSucActiva
End Enum

Enum eTipoTablaSincro
    etblMaestra
    etblTransaccion
    etblPendiente
End Enum

Enum eTipoCampo
    etblBusqueda
    etblExcepcion
End Enum

Enum eTipoError
    ErrGenerando
    ErrActualizando
    ErrOtros
End Enum

Enum eTipoActualizacion
    etaSoloAdm
    etaAdmPos
    etaSoloPos
End Enum

Enum eModalidad
    EnviaActualizaciones = 1
    RecibeRegistrosVentas = 2
    ActualizarCamposTransaccionesPlatino = 99
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Type StrucVentas
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
    Conexion                                                                            As ADODB.Connection
    sCodSucursal                                                                        As String
    sCodDeposito                                                                        As String
    sCodMoneda                                                                          As String
    nRegistrosLote                                                                      As Long
    GrabarDocumentoFiscal                                                               As Boolean
    sRutaArchivosOrigen                                                                 As String
    sCarpetaProcesados                                                                  As String
    sCarpetaProcesadosParcialmente                                                      As String
    sCarpetaNoProcesados                                                                As String
    sIdVentaCredito                                                                     As String
    InfoMonedas                                                                         As Collection
    InfoDenominaciones                                                                  As Collection
    DebugMode                                                                           As Boolean
    PoliticaCliente15yUltimo_5Dias                                                      As Boolean
    DiasVencimientoFacturaCxC                                                           As Long
    
End Type

Type StrucActualizaciones
    Habilitado                                                                          As Boolean
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
    sStoreID                                                                            As String
    sRutaArchivosDestino                                                                As String
    sRutaArchivosTmp                                                                    As String
    Conexion                                                                            As ADODB.Connection
    nRegistrosLote                                                                      As Long
    FuelDepartmentID                                                                    As String
    TaxField                                                                            As String
    TaxCollection                                                                       As Collection
    DistinguirCodigosDeBarraComo                                                        As String
End Type

Type StrucConex
    sModalidad                                                                          As String
    sArchivo                                                                            As String
    sServidor                                                                           As String
    sUsuario                                                                            As String
    sPassword                                                                           As String
    sBD                                                                                 As String
    sConnectTimeOut                                                                     As Long
    sCommandTimeOut                                                                     As Long
    mConexion                                                                           As ADODB.Connection
    nRegistrosLote                                                                      As Long
    SeparadorDecimalArchivos                                                            As String
End Type

Private mClsTmp As Object

Public mEstrucConex                                                                     As StrucConex
Public mEstrucAct                                                                       As StrucActualizaciones
Public mEstrucVentas                                                                    As StrucVentas

Public gSucursalPrincipal                                                               As String
Public gEquipo                                                                          As String
Public gCarpetaDatos                                                                    As String
Public gDec                                                                             As String

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String
Global Const Platino_StellarVendorName = "BWS" ' (BIGWISE Stellar)

Const SWP_NOSIZE = &H1
Const SWP_NOMOVE = &H2
Const SWP_NOZORDER = &H4
Const SWP_NOREDRAW = &H8
Const SWP_NOACTIVATE = &H10
Const SWP_DRAWFRAME = &H20
Const SWP_SHOWWINDOW = &H40
Const SWP_HIDEWINDOW = &H80
Const SWP_NOCOPYBITS = &H100
Const SWP_NOREPOSITION = &H200

'***************************************** Rutinas Api ******************************************************************

Public Function ObtenerConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String) As String
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 9999, sIniFile)
    ObtenerConfiguracion = Left$(sTemp, nLength)
End Function

Public Sub EscribirConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String)
    WritePrivateProfileString sSection, sKey, sDefault, sIniFile
End Sub

Public Function CadenasIguales(pCad1 As String, pCad2 As String) As Boolean
    CadenasIguales = Trim(UCase(pCad1)) = Trim(UCase(pCad2))
End Function

Public Function ObtenerNombreEquipo() As String
    
    Dim sEquipo As String * 255
    
    GetComputerName sEquipo, 255
    ObtenerNombreEquipo = Replace(sEquipo, Chr(0), "")
    
End Function

Public Function MoverArchivo(pOrigen As String, pDestino As String) As Boolean
    'Debug.Print pOrigen & "->" & pDestino
    MoverArchivo = MoveFile(pOrigen, pDestino)
End Function

Public Function CopiarArchivo(pOrigen As String, pDestino As String, pSobreEscribir As Boolean) As Boolean
    'MsgBox pOrigen & "->" & pDestino
    CopiarArchivo = CopyFile(pOrigen, pDestino, IIf(pSobreEscribir, 0, 1))
End Function

'***************************************************************Metodos Principales ***************************************

Sub Main()
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    gDec = SystemDecSymbol
    
    IniciarConfiguracion
    IniciarAgente
    
End Sub

Private Sub IniciarConfiguracion()
    
    With mEstrucConex
        
        .sArchivo = App.Path & "\Setup.ini"
        
        .sModalidad = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "Modalidad", "1"))
        
        ' -------- Configuraci�n de SRV_STELLAR
        
        .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "CommandTimeOut", "30"))
        .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "SERVER", "ConnectionTimeOut", "15"))
        
        .sServidor = ObtenerConfiguracion(.sArchivo, "SERVER", "SRV_LOCAL", "")
        
        'If .sModalidad = EnviaActualizaciones Then
            '.sBD = "VAD10"
        'ElseIf .sModalidad = RecibeRegistrosVentas Then
            .sBD = "VAD10"
        'End If
        
        .sUsuario = ObtenerConfiguracion(.sArchivo, "SERVER", "User", "SA")
        .sPassword = ObtenerConfiguracion(.sArchivo, "SERVER", "Password", "")
        
        .SeparadorDecimalArchivos = ObtenerConfiguracion(.sArchivo, "SERVER", "SeparadorDecimalArchivos", ".")
        
        If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
            End If
            
        End If
        
    End With
    
    If mEstrucConex.sModalidad = EnviaActualizaciones Then
        
        With mEstrucAct
            
            .sArchivo = mEstrucConex.sArchivo
            
            .sStoreID = ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "StoreID", "")
            
            If Trim(.sStoreID) = vbNullString Then
                Call EscribirLog("ID de Sucursal / Tienda inv�lida / no establecida.")
                End
            End If
            
            .sRutaArchivosDestino = ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "RutaArchivosDestino", "")
            
            If Trim(.sRutaArchivosDestino) = vbNullString Then
                Call EscribirLog("RutaArchivosDestino inv�lida / no establecida.")
                End
            End If
            
            .sRutaArchivosTmp = ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "RutaArchivosTmp", "$(AppPath)")
            
            .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "CommandTimeOut", "-1"))
            .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "ConnectionTimeOut", "-1"))
            
            If .sCommandTimeOut = -1 Then .sCommandTimeOut = mEstrucConex.sCommandTimeOut
            If .sConnectTimeOut = -1 Then .sConnectTimeOut = mEstrucConex.sConnectTimeOut
            
            .sServidor = ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "SRV_LOCAL", vbNullString)
            If .sServidor = vbNullString Then .sServidor = mEstrucConex.sServidor
            
            .sBD = UCase(ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "BD", "VAD10"))
            
            .sUsuario = ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "User", vbNullString)
            .sPassword = ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "Password", vbNullString)
            
            If .sUsuario = vbNullString And .sPassword = vbNullString Then
                .sUsuario = mEstrucConex.sUsuario
                .sPassword = mEstrucConex.sPassword
            End If
            
            If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
                
                Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
                
                If Not mClsTmp Is Nothing Then
                    .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                    .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
                End If
                
            End If
            
            .nRegistrosLote = CLng(Val(ObtenerConfiguracion(.sArchivo, "ENV_ACTUALIZACIONES", "RegistrosLote", "-1")))
            If (.nRegistrosLote <= 0) Then .nRegistrosLote = 30
            mEstrucConex.nRegistrosLote = .nRegistrosLote
            
            .FuelDepartmentID = ObtenerConfiguracion(mEstrucConex.sArchivo, "ENV_ACTUALIZACIONES", "FuelDepartmentID", "[*UNDEFINED*]")
            .TaxField = ObtenerConfiguracion(mEstrucConex.sArchivo, "ENV_ACTUALIZACIONES", "TaxField", "")
            Set .TaxCollection = ConvertirCadenaDeAsociacion(.TaxField)
            .DistinguirCodigosDeBarraComo = ObtenerConfiguracion(mEstrucConex.sArchivo, "ENV_ACTUALIZACIONES", "DistinguirCodigosDeBarraComo", "Bar:")
            
        End With
        
    ElseIf mEstrucConex.sModalidad = RecibeRegistrosVentas Then
        
        With mEstrucVentas
            
            .sArchivo = mEstrucConex.sArchivo
            
            ' -------- Configuraci�n de SRV_VENTAS
            
            .sRutaArchivosOrigen = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "RutaArchivosOrigen", "")
            
            If Trim(.sRutaArchivosOrigen) = vbNullString Then
                Call EscribirLog("RutaArchivosOrigen inv�lida / no establecida.")
                End
            End If
            
            .sCarpetaProcesados = FindPath("CorrectamenteProcesados", NO_SEARCH_MUST_FIND_EXACT_MATCH) 'App.Path & "\CorrectamenteProcesados"
            .sCarpetaProcesadosParcialmente = FindPath("ParcialmenteProcesados", NO_SEARCH_MUST_FIND_EXACT_MATCH) 'App.Path & "\ParcialmenteProcesados"
            .sCarpetaNoProcesados = FindPath("NoProcesados", NO_SEARCH_MUST_FIND_EXACT_MATCH) 'App.Path & "\NoProcesados"
            
            CreateFullDirectoryPath .sCarpetaProcesados
            CreateFullDirectoryPath .sCarpetaProcesadosParcialmente
            CreateFullDirectoryPath .sCarpetaNoProcesados
            
            .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "CommandTimeOut", "-1"))
            .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "ConnectionTimeOut", "-1"))
            
            If .sCommandTimeOut = -1 Then .sCommandTimeOut = mEstrucConex.sCommandTimeOut
            If .sConnectTimeOut = -1 Then .sConnectTimeOut = mEstrucConex.sConnectTimeOut
            
            .sServidor = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "SRV_LOCAL", vbNullString)
            If .sServidor = vbNullString Then .sServidor = mEstrucConex.sServidor
            
            .sBD = UCase(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "BD", "VAD10"))
            
            .sCodSucursal = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "CodSucursal", "")
            
            If Len(.sCodSucursal) = 0 Then
                Call EscribirLog("Sucursal de venta inv�lida / no establecida.")
'                End
            End If
            
            .sCodDeposito = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "CodDeposito", "")
            
            If Len(.sCodDeposito) = 0 Then
                Call EscribirLog("Dep�sito de venta inv�lido / no establecido.")
                End
            End If
            
            .sCodMoneda = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "CodMoneda", "")
            
            If Len(.sCodMoneda) = 0 Then
                Call EscribirLog("C�digo de Moneda inv�lido / no establecido.")
                End
            End If
            
            .sIdVentaCredito = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "IdVentaCredito", "12")
            
            sMonedas = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "Monedas", "")
            
            Set .InfoMonedas = ConvertirCadenaDeAsociacion(sMonedas)
            
            sDenominaciones = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "Denominaciones", "")
            
            Set .InfoDenominaciones = ConvertirCadenaDeAsociacion(sDenominaciones)
            
            .GrabarDocumentoFiscal = (Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "GrabarDocumentoFiscal", "1")) = 1)
            
            .sUsuario = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "User", vbNullString)
            .sPassword = ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "Password", vbNullString)
            
            If .sUsuario = vbNullString And .sPassword = vbNullString Then
                .sUsuario = mEstrucConex.sUsuario
                .sPassword = mEstrucConex.sPassword
            End If
            
            If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
                
                Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
                
                If Not mClsTmp Is Nothing Then
                    .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                    .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
                End If
                
            End If
            
            .nRegistrosLote = CLng(Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "RegistrosLote", "-1")))
            If (.nRegistrosLote <= 0) Then .nRegistrosLote = 50
            mEstrucConex.nRegistrosLote = .nRegistrosLote
            
            .DebugMode = (Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "DebugMode", "")) = 1)
            
            .PoliticaCliente15yUltimo_5Dias = (Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "PoliticaCliente_VencimientoCxc_15yUltimo_5Dias", "0")) = 1)
            
            ' 0 -> Expira el mismo d�a. > 0 -> Expira a los n D�as siguientes.
            .DiasVencimientoFacturaCxC = Abs(Val(ObtenerConfiguracion(.sArchivo, "REC_VENTAS", "DiasVencimientoFacturaCxC", "0")))
            
        End With
        
    ElseIf mEstrucConex.sModalidad = ActualizarCamposTransaccionesPlatino Then
        
        With mEstrucVentas
            
            .sArchivo = mEstrucConex.sArchivo
            
            ' -------- Configuraci�n de SRV_VENTAS
            
            .sRutaArchivosOrigen = ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "RutaArchivosOrigen", "")
            
            If Trim(.sRutaArchivosOrigen) = vbNullString Then
                Call EscribirLog("RutaArchivosOrigen inv�lida / no establecida.")
                End
            End If
            
            .sCarpetaProcesados = FindPath("Procesados", NO_SEARCH_MUST_FIND_EXACT_MATCH, .sRutaArchivosOrigen)
            
            CreateFullDirectoryPath .sCarpetaProcesados
            
            .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "CommandTimeOut", "-1"))
            .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "ConnectionTimeOut", "-1"))
            
            If .sCommandTimeOut = -1 Then .sCommandTimeOut = mEstrucConex.sCommandTimeOut
            If .sConnectTimeOut = -1 Then .sConnectTimeOut = mEstrucConex.sConnectTimeOut
            
            .sServidor = ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "SRV_LOCAL", vbNullString)
            If .sServidor = vbNullString Then .sServidor = mEstrucConex.sServidor
            
            .sBD = UCase(ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "BD", "VAD10"))
            
            .sUsuario = ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "User", vbNullString)
            .sPassword = ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "Password", vbNullString)
            
            If .sUsuario = vbNullString And .sPassword = vbNullString Then
                .sUsuario = mEstrucConex.sUsuario
                .sPassword = mEstrucConex.sPassword
            End If
            
            If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
                
                Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
                
                If Not mClsTmp Is Nothing Then
                    .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                    .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
                End If
                
            End If
            
            .nRegistrosLote = CLng(Val(ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "RegistrosLote", "-1")))
            If (.nRegistrosLote <= 0) Then .nRegistrosLote = 50
            mEstrucConex.nRegistrosLote = .nRegistrosLote
            
            .DebugMode = (Val(ObtenerConfiguracion(.sArchivo, "REC_CAMPOS_XML", "DebugMode", "")) = 1)
            
        End With
        
    End If
    
    'gSucursalPrincipal = ObtenerConfiguracion(.sArchivo, "Entrada", "DefaultBranch", "")
    gEquipo = ObtenerNombreEquipo()
    'gCarpetaDatos = ObtenerConfiguracion(.sArchivo, "Entrada", "CarpetaDatos", App.Path & "\Datos\")
    
End Sub

Private Sub IniciarAgente()
    
    Dim mClsAgente As clsAgente, NewUser As String, NewPassword As String
    
    With mEstrucConex
        
        NewUser = vbNullString: NewPassword = vbNullString
        
        If EstablecerConexion(.mConexion, .sServidor, .sBD, .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
            
            If NewUser <> "" Or NewPassword <> "" Then
                EscribirConfiguracion .sArchivo, "SERVER", "User", NewUser
                EscribirConfiguracion .sArchivo, "SERVER", "Password", NewPassword
            End If
            
            If UCase(mEstrucAct.sUsuario) = "SA" And Len(mEstrucAct.sPassword) = 0 Then
                EscribirConfiguracion .sArchivo, "ENV_ACTUALIZACIONES", "User", NewUser
                EscribirConfiguracion .sArchivo, "ENV_ACTUALIZACIONES", "Password", NewPassword
                mEstrucAct.sUsuario = .sUsuario
                mEstrucAct.sPassword = .sPassword
            End If
            
            If UCase(mEstrucVentas.sUsuario) = "SA" And Len(mEstrucVentas.sPassword) = 0 Then
                
                If .sModalidad = ActualizarCamposTransaccionesPlatino Then
                    EscribirConfiguracion .sArchivo, "REC_CAMPOS_XML", "User", NewUser
                    EscribirConfiguracion .sArchivo, "REC_CAMPOS_XML", "Password", NewPassword
                ElseIf .sModalidad = RecibeRegistrosVentas Then
                    EscribirConfiguracion .sArchivo, "REC_VENTAS", "User", NewUser
                    EscribirConfiguracion .sArchivo, "REC_VENTAS", "Password", NewPassword
                End If
                
                mEstrucVentas.sUsuario = .sUsuario
                mEstrucVentas.sPassword = .sPassword
                
            End If
            
        End If
        
    End With
    
    If mEstrucConex.sModalidad = EnviaActualizaciones Then
        With mEstrucAct
            
            NewUser = vbNullString: NewPassword = vbNullString
            
            If Not (CadenasIguales(mEstrucConex.sServidor, .sServidor) _
            And CadenasIguales(mEstrucConex.sUsuario, .sUsuario) _
            And CadenasIguales(mEstrucConex.sPassword, .sPassword)) _
            Then
                If EstablecerConexion(.Conexion, .sServidor, .sBD, .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
                    If NewUser <> "" Or NewPassword <> "" Then
                        EscribirConfiguracion .sArchivo, "ENV_ACTUALIZACIONES", "User", NewUser
                        EscribirConfiguracion .sArchivo, "ENV_ACTUALIZACIONES", "Password", NewPassword
                    End If
                End If
            Else
                Set .Conexion = mEstrucConex.mConexion
            End If
            
        End With
    ElseIf mEstrucConex.sModalidad = RecibeRegistrosVentas Then
        With mEstrucVentas
            
            NewUser = vbNullString: NewPassword = vbNullString
            
            If Not (CadenasIguales(mEstrucConex.sServidor, .sServidor) _
            And CadenasIguales(mEstrucConex.sUsuario, .sUsuario) _
            And CadenasIguales(mEstrucConex.sPassword, .sPassword)) _
            Then
                If EstablecerConexion(.Conexion, .sServidor, .sBD, .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
                    If NewUser <> "" Or NewPassword <> "" Then
                        EscribirConfiguracion .sArchivo, "REC_VENTAS", "User", NewUser
                        EscribirConfiguracion .sArchivo, "REC_VENTAS", "Password", NewPassword
                    End If
                End If
            Else
                Set .Conexion = mEstrucConex.mConexion
            End If
            
        End With
    ElseIf mEstrucConex.sModalidad = ActualizarCamposTransaccionesPlatino Then
        With mEstrucVentas
            
            NewUser = vbNullString: NewPassword = vbNullString
            
            If Not (CadenasIguales(mEstrucConex.sServidor, .sServidor) _
            And CadenasIguales(mEstrucConex.sUsuario, .sUsuario) _
            And CadenasIguales(mEstrucConex.sPassword, .sPassword)) _
            Then
                If EstablecerConexion(.Conexion, .sServidor, .sBD, .sConnectTimeOut, , , .sUsuario, .sPassword, True, NewUser, NewPassword) Then
                    If NewUser <> "" Or NewPassword <> "" Then
                        EscribirConfiguracion .sArchivo, "REC_CAMPOS_XML", "User", NewUser
                        EscribirConfiguracion .sArchivo, "REC_CAMPOS_XML", "Password", NewPassword
                    End If
                End If
            Else
                Set .Conexion = mEstrucConex.mConexion
            End If
            
        End With
    End If
    
    With mEstrucConex
        If Not .mConexion Is Nothing Then
            If .mConexion.State = adStateOpen Then
                Set mClsAgente = New clsAgente
                mClsAgente.IniciarAgente .mConexion
                Set mClsAgente = Nothing
            End If
        End If
    End With
    
    End
    
End Sub

'*********************************************** Funciones de Apoyo ****************************************************************

Public Function EstablecerConexion(ByRef pCn As ADODB.Connection, pServidor As String, pBD As String, _
Optional pCnnTOut As Long = 15, Optional pCorrelativo As String, Optional pSucursal As String, _
Optional ByRef pUser, Optional ByRef pPassword, Optional AuthReset As Boolean = False, _
Optional ByRef NewUser As String, Optional ByRef NewPassword As String) As Boolean
    
Retry:
    
    On Error GoTo ErrHandler
    
    Dim mCadena As String
    Dim mServidor As String
    
    Set pCn = New ADODB.Connection
    pCn.ConnectionTimeout = pCnnTOut
    
    If IsMissing(pUser) Or IsMissing(pPassword) Then
        pCn.Open CadenaConexion(pServidor, pBD)
    Else
        pCn.Open CadenaConexion(pServidor, pBD, CStr(pUser), CStr(pPassword))
    End If
    
    EstablecerConexion = True
    
    Exit Function
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If AuthReset Then
        
        If Err.Number = -2147217843 Then
                    
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If mClsTmp Is Nothing Then GoTo UnhandledErr
            
            TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
            
            If Not IsEmpty(TmpVar) Then
                pUser = TmpVar(0): pPassword = TmpVar(1)
                NewUser = TmpVar(2): NewPassword = TmpVar(3)
                Resume Retry
            End If
            
            Set mClsTmp = Nothing
            
        End If
        
    End If
    
UnhandledErr:
    
    GrabarErrorBD "EstablecerConexion", mErrDesc, mErrNumber, ErrOtros
    
End Function

Private Function CadenaConexion(ByVal pServidor As String, ByVal pBD As String, Optional ByVal pUser As String = "SA", Optional ByVal pPassword As String = "")
    CadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" & pBD & ";Data Source=" & pServidor & ";" _
    & IIf(pUser = vbNullString Or pPassword = vbNullString, _
    "Persist Security Info=False;User ID=" & pUser & ";", _
    "Persist Security Info=True;User ID=" & pUser & ";Password=" & pPassword & ";")
    'Debug.Print CadenaConexion
End Function

Public Function Correlativo(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mValor As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * From MA_CORRELATIVOS WHERE (CU_Campo = '" & pCampo & "')"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockPessimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_Valor = mRs!nu_Valor + 1
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
        mRs.Update
    Else
        mRs.AddNew
        mRs!cu_Campo = pCampo
        mRs!cu_Formato = "0000000000"
        mRs!nu_Valor = 1
        mRs.Update
        mValor = Format(mRs!nu_Valor, mRs!cu_Formato)
    End If
    
    Correlativo = mValor
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    EscribirLog "(ObtenerCorrelativo)" & " " & Err.Description, Err.Number
    
End Function

Public Sub EscribirLog(ByVal pMensaje As String, Optional ByVal pNumError As Long)
    
    Dim mCanal As Integer
    Dim mArchivo As String
    
    mCanal = FreeFile()
    Open App.Path & "\Errores.log" For Append Access Write As #mCanal
    Print #mCanal, Now & ", " & pMensaje & IIf(pNumError > 0, " (Error Numero: " & pNumError & ")", "")
    Close #mCanal
    
End Sub

Public Sub VerificarExisteCarpeta(pCarpeta As String)
    If Dir(pCarpeta, vbDirectory) = "" Then
        MkDir pCarpeta
    End If
End Sub

Public Sub GrabarErrorBD(ByVal pRutina As String, ByVal pError As String, _
ByVal pNError As Long, pTipo As eTipoError, Optional ByVal pCorrida As String, _
Optional ByVal pSucursal, Optional ByVal pTabla As String, Optional ByVal pCodigoDoc As String, _
Optional ByVal pConcepto As String, Optional ByVal pFecha As Date = "01/01/1900")
    
    Dim mSQL As String
    
    On Error GoTo Errores
    
    modAgente.EscribirLog gEquipo & "|" & pError & "|" & pRutina & "|" & pTabla & "|" & CStr(pTipo) & "|" & pCorrida, pNError
    
    mSQL = "Insert into MA_ERRORCORRIDAS_AGENTE_ADM (equipo, corrida, error, nerror, sucursal, " _
    & "tabla, codigo, concepto, fecha,rutina, tipo) values " _
    & "('" & gEquipo & "','" & pCorrida & "','" & Left(Replace(pError, "'", ""), 120) & "'," & CStr(pNError) & ",'" & CStr(pSucursal) & "', " _
    & "'" & pTabla & "','" & pCodigoDoc & "','" & pConcepto & "','" & FechaBD(pFecha) & "','" & pRutina & "'," & CStr(pTipo) & ")"
    
    mEstrucConex.mConexion.Execute mSQL
   
    Exit Sub
    
Errores:
    
    modAgente.EscribirLog "Error Grabando Tabla Errores: " & Err.Description, Err.Number
    Err.Clear
    
End Sub

Public Function ExisteTabla(ByVal pTabla As String, pCn As ADODB.Connection, Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTabla = Not (pRs.EOF And pRs.BOF)
End Function

Public Function ExisteCampoTabla(ByVal pColumna As String, ByVal pTabla As String, pCn As ADODB.Connection, _
Optional ByVal pBD As String = "") As Boolean
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Column_Name FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE Table_Name = '" & pTabla & "' AND Column_Name = '" & pColumna & "'")
    ExisteCampoTabla = Not (pRs.EOF And pRs.BOF)
End Function

Public Function ExisteCampoRs(pRs As ADODB.Recordset, pCampo As String) As Boolean
    
    On Error GoTo Errores
    
    ValorTmp = pRs.Fields(pCampo).Value
    ExisteCampoRs = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Sub VentanaVisible(ByRef pFrm As Object, Optional pTop As Boolean = False)
    If pTop Then
        'SetWindowPos pFrm.hWnd, -1, pFrm.Left / Screen.TwipsPerPixelX, pFrm.Top / Screen.TwipsPerPixelY, pFrm.Width / Screen.TwipsPerPixelX, pFrm.Height / Screen.TwipsPerPixelY, SWP_NOACTIVATE Or SWP_NOSIZE Or SWP_NOMOVE
        SetWindowPos pFrm.hWnd, -1, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOSIZE
    Else
        SetWindowPos pFrm.hWnd, -2, 0, 0, 0, 0, SWP_NOACTIVATE Or SWP_NOSIZE
    End If
End Sub

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

' Imprime un recordset o el Record Actual utilizando el m�todo original de ADODB.

' El Original de ADODB pero incluyendo los headers.
' Es m�s r�pido pero menos ordenado.
' NO Deja el recordset en su posici�n del cursor original:
' Lo lleva al EOF si es el Recordset, o avanza el cursor una posici�n si es solo el Record Actual.
' Por lo cual habr�a que hacer MoveFirst o MovePrevious respectivamente
' Dependiendo del tipo de recorset para poder volver a recorrerlo.

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, Optional OnlyCurrentRow As Boolean = False) As String
    
    On Error Resume Next
    
    Dim Data As Boolean
    Dim RsPreview As String
    Dim RowData As String
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    For i = 0 To pRs.Fields.Count - 1
        RsPreview = RsPreview + IIf(i = 0, vbNullString, "|") + pRs.Fields(i).Name
    Next i
    
    RsPreview = RsPreview & vbNewLine
    
    If Data Then
        If OnlyCurrentRow Then
            RowData = pRs.GetString(, 1, "|", vbNewLine, "NULL")
        Else
            RowData = pRs.GetString(, , "|", vbNewLine, "NULL")
        End If
    End If
    
    RsPreview = RsPreview & RowData
    
    QuickPrintRecordset = RsPreview
    
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = QuickPrintRecordset(pRs, True)
End Function

' Imprime un Recordset o el Record Actual de manera ordenada. Deja al recordset en su posici�n (Record) Original.

' Parametros:

' 1) pRs: Recordset
' 2) pRowNumAlias: Alias para una columna que enumera los rows. Si no se especifica o OnlyCurrentRow no se imprime
' 3) OnlyCurrentRow: Imprimir solo el record actual o todo.
' 4) EncloseHeaders: Encierra los cabeceros en Corchetes para mayor separaci�n.
' 5) LineBeforeData: Agrega una Linea de separaci�n entre Header y Records.
' 6) pOnValueError: Que se imprime si da un error a la hora de recuperar un campo.
' 7) Uso interno: Dejar siempre el valor default. Es para poder imprimir los Recordsets anidados (Tipo Consulta Shape).

' Recomendaci�n al obtener el String devuelto: Copiar y Pegar en TextPad o un Bloc de Notas sin Line Wrap para analizar.

Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
Optional NestLevel As Byte = 0) As String
    
    On Error GoTo FuncError
    
    Dim RsPreview As String, bRowNumAlias As Boolean, RowNumAlias As String, RowNumCount As Double
    Dim OriginalRsPosition, TmpRsValue, mColLength() As Double, mColArrIndex, pRsValues As ADODB.Recordset
    Dim Data As Boolean
    
    If OnlyCurrentRow Then pRowNumAlias = vbNullString
    
    bRowNumAlias = (pRowNumAlias <> vbNullString)
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    ReDim mColLength(pRs.Fields.Count - 1 + IIf(bRowNumAlias, 1, 0))
    
    If bRowNumAlias Then
        RowNumAlias = IIf(EncloseHeaders, "[", vbNullString) & CStr(pRowNumAlias) & IIf(EncloseHeaders, "]", vbNullString)
        RowNumCount = 0
        RsPreview = RsPreview & GetTab(NestLevel) & "$(mCol[0])"
        mColLength(0) = Len(RowNumAlias)
    End If
    
    For i = 0 To pRs.Fields.Count - 1
        'RsPreview = RsPreview + pRs.Fields(i).Name + vbTab
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & "$(mCol[" & mColArrIndex & "])"
        mColLength(mColArrIndex) = Len(IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString))
    Next i
    
    If LineBeforeData Then RsPreview = RsPreview & vbNewLine
    
    If Data And Not OnlyCurrentRow Then
        OriginalRsPosition = SafeProp(pRs, "Bookmark", -1)
        If pRs.CursorType <> adOpenForwardOnly Then
            pRs.MoveFirst
        Else
            pRs.Requery
        End If
        If OriginalRsPosition <> -1 Then Set pRsValues = pRs.Clone(adLockReadOnly)
    Else
        Set pRsValues = pRs
    End If
    
    RowNumCount = 0
    
    Do While Not pRs.EOF
        
        'RsPreview = RsPreview & vbNewLine
        'If bRowNumAlias Then RsPreview = RsPreview & Rellenar_SpaceR(RowNumCount, Len(RowNumAlias), " ")
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            If Len(TmpRsValue) > mColLength(0) Then mColLength(0) = Len(TmpRsValue)
            'RsPreview = RsPreview & TmpRsValue
        End If
        
        For i = 0 To pRs.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRs, pRs.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
                        
            If UCase(TypeName(TmpRsValue)) <> UCase("Recordset") Then
                If Len(TmpRsValue) > mColLength(mColArrIndex) Then mColLength(mColArrIndex) = Len(TmpRsValue)
            End If
            'RsPreview = RsPreview & TmpRsValue
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRs.MoveNext
        
    Loop
    
    If bRowNumAlias Then RsPreview = Replace(RsPreview, "$(mCol[0])", Rellenar_SpaceR(RowNumAlias, mColLength(0), " ") & vbTab, , 1)
    
    For i = 0 To pRs.Fields.Count - 1
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = Replace(RsPreview, "$(mCol[" & mColArrIndex & "])", Rellenar_SpaceR( _
        IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString), _
        mColLength(mColArrIndex), " ") & vbTab, , 1)
    Next i
    
    If OriginalRsPosition = -1 And Not OnlyCurrentRow Then
        Set pRsValues = pRs
        If Data Then
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    RowNumCount = 0
    
    Do While Not pRsValues.EOF
        
        RsPreview = RsPreview & GetTab(NestLevel) & vbNewLine
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            RsPreview = RsPreview & GetTab(NestLevel) & Rellenar_SpaceR(TmpRsValue, mColLength(0), " ") & vbTab
        End If
        
        For i = 0 To pRsValues.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRsValues, pRsValues.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
            If Not UCase(TypeName(TmpRsValue)) = UCase("Recordset") Then
                RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & Rellenar_SpaceR(TmpRsValue, mColLength(mColArrIndex), " ") & vbTab
            Else
                RsPreview = RsPreview & vbNewLine & PrintRecordset(TmpRsValue, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel + 1)
            End If
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRsValues.MoveNext
        
    Loop
    
    PrintRecordset = RsPreview
    
Finally:
    
    On Error Resume Next
    
    If Data And Not OnlyCurrentRow Then
        If OriginalRsPosition <> -1 Then
            If pRs.CursorType = adOpenForwardOnly Then
                pRs.Requery
                pRs.Move OriginalRsPosition, 0
            Else
                SafePropAssign pRs, "Bookmark", OriginalRsPosition
            End If
        Else
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    Exit Function
    
FuncError:
    
    'Resume ' Debug
    
    PrintRecordset = vbNullString
    
    Resume Finally
    
End Function

' Imprime el Record Actual de un Recordset.

Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    PrintRecord = PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
End Function

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

Public Function GetServerName(pCn As ADODB.Connection) As String
    
    On Error GoTo ErrorSrv
    
    GetServerName = pCn.Execute("SELECT @@SERVERNAME AS ServerName")!ServerName
    
    Exit Function
    
ErrorSrv:
    
    GetServerName = vbNullString
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim DDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                DDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - DDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   DDate = DateAdd("s", -1, DDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(DDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                DDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - DDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   DDate = DateAdd("s", -1, DDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(DDate, "YYYY-MM-DD HH:mm:ss") ' & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' How to Use:

' 1) To Know if Value Exists:

'If Collection_FindValueIndex(pCollection, pValue) <> -1 Then

' 2) To Search All Instances (Indexes) of Value in the Collection. Quick Example:

' SearchIndex = Collection_FindValueIndex(pCollection, pValue) ' Start Looking from Index 1

' If SearchIndex <> -1 Then
    ' This should be in a kind of While Loop...
    ' Keep searching other matches starting from next position:
    'SearchIndex = Collection_FindValueIndex(pCollection, pValue, SearchIndex + 1)
    ' Until Search Index = -1
' Else
    ' Next Match not found...
' End if

Public Function Collection_FindValueIndex(pCollection As Collection, ByVal pValue, Optional ByVal pStartIndex As Long = 1) As Long
    
    On Error GoTo Error
    
    Dim i As Long
    
    Collection_FindValueIndex = -1
    
    For i = pStartIndex To pCollection.Count
        If SafeEquals(pCollection.Item(i), pValue) Then
            Collection_FindValueIndex = i
            Exit Function
        End If
    Next i
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteKey = Collection_HasKey(pCollection, pKey)
End Function

' Collection_ExisteKey V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pKey, Null)) Then ' Key might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pKey) ' If this doesn't Proc Error Key is Null, but exists.
        Collection_HasKey = True
    Else ' Key Exists AND <> NULL
        Collection_HasKey = True
    End If
    
    Exit Function
        
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasKey = False ' Only on error, Key is not contained in the Collection.
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteIndex = Collection_HasIndex(pCollection, pIndex)
End Function

' Collection_ExisteIndex V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pIndex, Null)) Then ' Index might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pIndex) ' If this doesn't Proc Error Index also exists.
        Collection_HasIndex = True
    Else ' Index Exists AND <> NULL
        Collection_HasIndex = True
    End If
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasIndex = False ' Only on error Index is not contained in the Collection.
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = Collection_SafeRemoveKey(pCollection, pKey)
End Function

' Ignore error even if the Key is not contained in the Collection.

Public Function Collection_SafeRemoveKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pKey
        
    Collection_SafeRemoveKey = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveKey = False ' Non - existant.
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

' Ignore error even if the Index is not contained in the Collection.

Public Function Collection_SafeRemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pIndex
        
    Collection_SafeRemoveIndex = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveIndex = False ' Non - existant.
    
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|") As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        ParClaveValor = Split(CStr(Item), ":", 2)               'Izq:Clave        'Der:Valor
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function DatosMoneda(pCodigo, pCn) As ADODB.Recordset
    
    On Error Resume Next
    
    Set DatosMoneda = New Recordset
    
    DatosMoneda.CursorLocation = adUseClient
    
    DatosMoneda.Open "SELECT * FROM MA_MONEDAS WHERE c_CodMoneda = '" & pCodigo & "'", _
    pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    Set DatosMoneda.ActiveConnection = Nothing
    
    If DatosMoneda.EOF Then Set DatosMoneda = Nothing
    
End Function

Public Function KillSecure(pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
     If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStr(1, pPath, "\")
    
    If Pos <> 0 Then
        GetDirectoryRoot = Left(pPath, Pos - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStrRev(pPath, "\")
    
    If Pos <> 0 Then
        GetDirParent = Left(pPath, Pos - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function SystemDecSymbol() As String
    Dim TestFloatingNumber As String
    TestFloatingNumber = CStr((0 + (1 / 2)))
    For i = 1 To Len(TestFloatingNumber)
        If Not Mid(TestFloatingNumber, i, 1) Like "[0-9]" Then
            SystemDecSymbol = SystemDecSymbol & Mid(TestFloatingNumber, i, 1)
        End If
    Next
    If SystemDecSymbol = vbNullString Then SystemDecSymbol = "."
End Function

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

Public Function EndOfDay(Optional ByVal pFecha) As Date
    If IsMissing(pFecha) Then pFecha = Now
    pFecha = SDate(CDate(pFecha))
    EndOfDay = DateAdd("h", 23, DateAdd("n", 59, DateAdd("s", 59, pFecha)))
End Function
